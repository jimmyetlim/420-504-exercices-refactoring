package ca.qc.claurendeau.exercice_03;

public class PhoneNumber {
	private String areaCode;
	private String number;

	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String officeNumber) {
		this.number = officeNumber;
	}
	
	public void setAreaCode(String officeAreaCode) {
		this.areaCode = officeAreaCode;
	}

	
	public String getTelephoneNumber() {
		return ("(" + areaCode + ") " + number);
	}
}
