package ca.qc.claurendeau.exercice_04;

import ca.qc.claurendeau.exercice_03.Person;

class Department {
	private String chargeCode;
	private Person manager;

	public Department(Person manager) {
		this.manager = manager;
	}
	public Person getManager() {
		return manager;
	}
}
