package ca.qc.claurendeau.exercice_04;

// Suppression du "middle-man"

public class Employee {
	private Department department;

	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department arg) {
		department = arg;
	}
}

// Quelque part dans le code client
// manager = john.getDepartment().getManager();

