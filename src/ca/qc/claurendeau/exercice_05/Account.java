package ca.qc.claurendeau.exercice_05;

// Introduction de méthode extérieure

import java.util.Date;

public class Account {
  @SuppressWarnings("deprecation")

  double schedulePayment() {
    Date previousDate = new Date();
	Date paymentDate = new Date(previousDate.getYear(), previousDate.getMonth(), previousDate.getDate() + 7);
    // Issue a payment using paymentDate.
    // ...
	return 0.0;
  }
}
