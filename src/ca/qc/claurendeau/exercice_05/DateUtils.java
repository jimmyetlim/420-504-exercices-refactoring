package ca.qc.claurendeau.exercice_05;

import java.util.Date;

public class DateUtils {

	public static Date nextWeek(Date date) {
		return new Date(date.getYear(), date.getMonth(), date.getDate() + 7);
	}

}
