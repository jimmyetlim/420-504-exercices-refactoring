package ca.qc.claurendeau.exercice_09;

// Remplacement du 'switch' en utilisant le polymorphisme

abstract class Bird {
	abstract double getSpeed();
	protected int voltage;

	protected int getBaseSpeed() {
		// STUB
		return 0;
	}

	protected int getBaseSpeed(int voltage) {
		// STUB
		return 0;
	}
}


