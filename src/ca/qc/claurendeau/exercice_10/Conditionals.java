package ca.qc.claurendeau.exercice_10;

import java.util.Date;

// Remaniement des expressions pour les simplifier

public class Conditionals {
	private Date date;
	private int quantity;
	private int winterRate;
	private int winterServiceCharge;
	private int summerRate;
	private Date SUMMER_START;
	private Date SUMMER_END;

	private boolean isSummer()
	{
		return (date.after(SUMMER_START) && date.before(SUMMER_END));
	}
	
	double charge() {
		int charge;

		if (!isSummer()) {
			charge = quantity * winterRate + winterServiceCharge;
		}
		else {
			charge = quantity * summerRate;
		}

		if (isSummer()) {
			charge = quantity * summerRate;
		}
		else {
			charge = quantity * winterRate + winterServiceCharge;
		}

		
		return charge;
	}

}
