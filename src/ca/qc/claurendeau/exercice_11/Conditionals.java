package ca.qc.claurendeau.exercice_11;

// Remaniement des expressions pour les simplifier

public class Conditionals {
	private int seniority;
	private int monthsDisabled;
	private boolean isPartTime;

	
	double disabilityAmount() {
		return isNotEligableForDisability() ? 0 : -1;
	}
	
	private boolean isNotEligableForDisability() {
		// TODO Auto-generated method stub
		return false;
	}

	double _disabilityAmount() {
		if (seniority < 2) {
			return 0;
		}
		if (monthsDisabled > 12) {
			return 0;
		}
		if (isPartTime) {
			return 0;
		}
		// compute the disability amount
		//...
		return -1;
	}

}
